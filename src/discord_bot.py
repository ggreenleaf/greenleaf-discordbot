from message_handler import MessageHandler
from command_repository import load

import discord


class DiscordBot(discord.Client):
	def __init__(self):
		super(DiscordBot,self).__init__()
		self.handler = MessageHandler()

	def	on_message(self, msg):
		user = msg.author
		reply = self.handler.get_response(msg)

		if reply and reply["content"]:
			self.send_message(reply["channel"],reply["content"])

		return

	def on_ready(self):
		print "Logged in as"
		print self.user.name
		print self.user.id
		print "-----------"

	def move_message(self,msg,prev_channel):
		"""delete a message from a channel then moves it to correct channel"""
		pass

	def delete_message(self,msg):
		pass

	def load_commands(self):
		load()
