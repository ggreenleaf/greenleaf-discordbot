import requests
import json
from command_repository import run_command

class MessageHandler(object):
    def __init__(self):
        with open("resources/roles.txt") as f:
            self.allowed_roles = f.read().split("\n")

        #override roles when user may not have role permission to run command
        with open("resources/allowed_users.json") as f:
            self.allowed_users = json.load(f)

    def change_allowed_roles(roles):
        with open("resources/roles.txt") as f:
            data = f.read()
        self.allowed_roles = data.split("\n")

    def allow_user_response(self,user):
        try:
            return user.id == self.allowed_users[user.name.lower()]
        except KeyError:
            return False

    def allow_role_response(self, roles):
        return any(role.name.lower() in self.allowed_roles for role in roles)

    def allow_response(self,user):
        """returns true if a user is allowed to run a bot command"""
        return self.allow_user_response(user) or self.allow_role_response(user.roles)


    def get_response(self, msg):
        reply = {}
        user = msg.author
        cmd, args = self.parse_message(msg)

        if self.allow_response(user) and msg.content.startswith("!"):
                 content = run_command(cmd.lower(), args)
                 if len(content) >= 2000:
                       content = "Response is to long"
                 
                 reply["content"] = content                 
                 reply["channel"] = msg.channel
        return reply


    def parse_message(self,msg):
        """parse the contents of a message into cmd and arguments"""
        try:
            cmd, args = msg.content.split(" ",1)
        except ValueError:
            cmd = msg.content.lower()
            args = None

        return cmd.lower(), args
