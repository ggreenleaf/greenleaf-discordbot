from discord_bot import DiscordBot
import netrc

if __name__ == "__main__":
    host = "discordbot"
    secrets = netrc.netrc()
    username,account,password = secrets.authenticators(host)

    bot = DiscordBot()

    bot.login(username,password)
    bot.load_commands()
    bot.run()
