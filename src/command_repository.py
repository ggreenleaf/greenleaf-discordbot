import requests
import json
from random import randint, sample
from datetime import datetime, timedelta
from search import Search


cmd_map = {}
custom_cmds = {}
# quote_time = datetime.now()
# qod = "\"Every other knowledge is harmful to him who does not have knowledge of goodness\" -Michel de Montaigne"

def invokable (f):
    global cmd_map
    name = "!{}".format(f.__name__)
    cmd_map[name] = f
    return f

def run_command(cmd, args):
    content = None
    if cmd in cmd_map.keys():
        content = cmd_map[cmd](args)
    elif cmd in custom_cmds.keys():
        content = custom_cmds[cmd]

    return content


@invokable
def commands(*args):
    cmd_list = ",".join([cmd for cmd in cmd_map.keys()+custom_cmds.keys()])
    return "Here is a list of commands {}".format(cmd_list)

@invokable
def twitch(*args):
    """returns the twitch channel of a given user if they are live"""
    user = args[0]
    url = "https://api.twitch.tv/kraken/streams/{}".format(user)
    res = requests.get(url)
    # res.raise_for_status()
    link = "https://twitch.tv/{}".format(user)
    try:
        live = res.json()["stream"]
    except KeyError:
        return "Sorry user could not be found"

    if live is None:
        return "Sorry {} is not live".format(user)
    else:
        return "{} is live! Watch Now! {}".format(user,link)

@invokable
def add(*args):
    global custom_cmds
    try:
        new_cmd, text = args[0].split(" ",1)
    except ValueError:
        return "Invalid syntax for adding command (Missing Text)"

    if new_cmd.startswith("!"):
        if new_cmd in custom_cmds.keys():
            return "Can't over write other commands"
        else:
            custom_cmds[new_cmd.lower()] = text
    else:
        return "Invalid syntax for adding commanding missing '!'"

    save()
    return

@invokable
def delete(*args):
    global custom_cmds
    cmd = args[0]
    if cmd in custom_cmds.keys():
        custom_cmds.pop(cmd,None)
        save()
        return "{} deleted".format(cmd)
    return

@invokable
def save(*args):
    """saves the list of commands / respones to a json file"""
    global custom_cmds
    with open("resources/commands.json","w") as f:
        json.dump(custom_cmds,f)

@invokable
def roll(*args):
    return "Rolled a {}".format(randint(1,6))

@invokable
def flip(*args):
    flip = randint(1,2)
    result = "HEADS" if flip % 2 else "TAILS"
    return "Flipped a coin! It was {}".format(result)

@invokable
def source(*args):
    return ("My source is online! Here you go!"
            "https://bitbucket.org/ggreenleaf/greenleaf-discordbot")

@invokable
def giphy(*args):
    #giphy search api requires phrases to be + instead of ' '
    q = args[0].replace(" ","+")
    api_key = "dc6zaTOxFJmzC"
    uri = ("http://api.giphy.com/v1/gifs/search"
                 "?q={}&limit=1&offset=0&api_key={}").format(q,api_key)
    res = requests.get(uri)
    message = "Sorry I had trouble finding a gif :("
    if res.json()["data"]:
        message = res.json()["data"][0]["images"]["original"]["url"]
    return message    

@invokable
def solve(*args):
    '''solves a simple tile moving game'''
    search = "A_STAR"
    cost_flag = False
    
    def is_valid_start(s):
	    #board must be odd numbers and less then 13
	    if len(s) % 2 == 0 or (0 < len(s) > 13):
		    return False
	    #count of b and w must be the same and only 1 x must occur
	    return (s.count("w") == s.count("b") and s.count("x") == 1)        
    
    try:
        board = args[0].lower()             
    except TypeError:
        return "No starting board"
    
    if is_valid_start(board):
        s = Search(board,search,cost_flag)
        moves = s.search()    
        # "step %i: " % i, "move %i"% move_list[i].tag.index("x"), move_list[i].tag
        move_list = []
        for i in xrange(1,len(moves)):
            move_list.append("step {}: move {} {}".format(i,moves[i].tag.index("x")+1,moves[i].tag))
        return "\n".join(move_list)
    else:
        return "Invalid starting board"       
               
         
                   
@invokable
def lottery(*args):
   picks = sample(xrange(1,70),5)
   powerball = randint(1,26)
   picks_formatted = " ".join(map(str,sorted(picks)))
   results = "{} pball: {}".format(picks_formatted,powerball)
   return results     



def load(*args):
    """load the list of custom command entries into custom_cmds"""
    global custom_cmds
    with open("resources/commands.json") as f:
        custom_cmds = json.load(f)
        



