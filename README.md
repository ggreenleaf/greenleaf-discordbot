# Python Discord Bot #
## Description ##
Discord bot is a simple bot that interacts with a discord server. 
The bot uses the discord.py python API to interact with the chat which can be found here
[Rapptz Discord.py](https://github.com/Rapptz/discord.py)
Currently the bot allows for adding custom commands that send text messages. 
Todo bot will support scripting interface to delete, move, edit certain messages based on user programmed rules. 

## Downloading /Running ##

Currently the bot runs in a python virtualenv

First create a virtualenv that will contain the cloned repo. 
start the virtualenv and clone the git repo and install the required modules using the following command

```
#!bash

sudo pip install requirements -r
```

### Setting up login information ###
The bot uses the netrc module for loading login information
in your home directory add the login user email and password to the .netrc

### Commands.json and roles.txt ###
Before running the bot for the first time make sure you add a roles.txt file to the root of your cloned repo. The roles.txt file contains the roles that your server uses for example if you want everyone to be able to run bot commands then your roles.txt file will have one like everyone. Each role should exist on its own line. Custom added commands are stored in the commands.json.

## Running the bot ##
After setting up login and roles information you can run bot by typing 

```
#!python

python src/main.py
```

The bot will display in the terminal if it successfully logged in. 

## Supported Commands ##
Currently the bot supports the following commands which can be typed as a message in discord. 

### !add ###
!add <!new command> <text> will allow a user to add custom text responses to the bot. For example if you send the following message "!add !newyear Happy New Years!" Then next time !newyear is sent the bot will respond Happy New Years!.

### !twitch ###
!twitch <user> will check if the following user is live. If the user is live the bot returns a link to the channel if not it returns the user is not online. 

### !quote ###
!quote grabs the quote of the day from theysaidso.com and displays it in the chat. 


 

 

